import './SideNav.css'
import { Link } from "react-router-dom";

const { Component } = require("react");

export default class SideNav extends Component{

    render(){
        return(<div
          class="side-navbar active-nav"
          id="sidebar"
        >
          <ul class="nav flex-column text-white w-100">
            <a href="#" class="nav-link h1 text-white my-2">
              ADMIN PANEL
            </a>
            <li href="" class="nav-link text-left btn btn-primary mb-2">
            <box-icon  type="solid" color="white" size="xs" name="spreadsheet"></box-icon>
              <span class="mx-2">Blogs</span>
            </li>
            <Link to={"/Admin"}>
            <li href="#" class="nav-link text-left btn btn-primary mb-2">
            
              <box-icon  type="solid" color="white" size="xs" name="user"></box-icon>
              <span class="mx-2 text-white">Admin List</span>
            
            </li></Link>
            <li href="#" class="nav-link text-left btn btn-primary mb-2">
            <box-icon  type="solid" color="white" size="xs" name="user-detail"></box-icon>
              <span class="mx-2">Subscribers</span>
            </li>
            <li href="#" class="nav-link text-left btn btn-primary mb-2">
            <box-icon  type="solid" color="white" size="xs" name="inbox"></box-icon>
              <span class="mx-2">Inbox</span>
            </li>
            <li href="#" class="nav-link text-left btn btn-primary mb-2">
            <box-icon  type="solid" color="white" size="xs" name="dashboard"></box-icon>
              <span class="mx-2">Home Page</span>
            </li>
          </ul>
    
          
        </div>)
    }
}