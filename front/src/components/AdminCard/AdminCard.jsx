import './AdminCard.css'
import unnamed from '../../image/unnamed.png'
import {Link} from "react-router-dom"
const { Component } = require("react");



export default class AdminCard extends Component{

  state = {
    
    id: this.props.id,
    email: this.props.email,
    password: this.props.password
    
}

    render(){
      let { id, email, password,deleteAdmin} = this.props;
        return(
    
            <div class="card mt-4">
            <div class="row no-gutters m-1">
                <div class="col-auto">
                  {<img src={unnamed} class="UserImage" /> }
                </div>
                <div class="col">
                    <div class="block px-2">
                        <h4 class="title">{email}</h4>
                        <p class="text">{password}</p>
                        <button class="list-inline-item btn editbtn text-light ">
                           Edit
                        </button>
                        <Link to="/Admin/update" params={{id,email,password}}>
                        <button class="list-inline-item btn deletebtn text-light" onClick={() => deleteAdmin(id)}>
                          Delete
                        </button></Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    
    }}