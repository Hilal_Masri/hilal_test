import './EditAdmin.css'
import { withRouter, Switch, Route, Link } from "react-router-dom";
const { Component } = require("react");

export default class EditAdmin extends Component{
    state={
        id: 17,
        email:" hilal1922001@gmail.com",
        password: "123"
    }

    updateAdmin = async (id, params) => {
        let { email, password } = params;
        let url = `//localhost:8000/Admins/update/${id}`;
        let paramsErr = "you need at least name or email properties to update a contact";
    
        // create our url
        if (!email && !password) throw new Error(paramsErr);
        else if (email && !password) url += `/?email=${email}`;
        else if (!email && password) url += `/?password=${password}`;
        else if (email && password) url += `/?email=${email}&password=${password}`;
    
        try {
          const response = await fetch(url);
        }catch(err){
            this.setState({ error_message: err })
        }
    }    
    
    handleChange = (event) => {
        let { name, value } = event.target;
        this.setState({ [name]: value });
    }
    
    handleSubmit = (event) => {
        event.nativeEvent.preventDefault();
        let { email, password } = this.state;
        this.updateAdmin({ email, password });
        document.getElementById("email").value = "";
        document.getElementById("password").value = "";
    }

    render(){
        let {id, email, password ,error} = this.state;
        return  (
            
                <div>
            <div class="container" id="container" >
                 <h1 class=" mb-4 text-light">EDIT ADMIN</h1>
                 <form onSubmit={this.handleSubmit}>
                 <div class="form-group" id="Email">
                        <label for="email">Email</label>
                        <input type="email"
                        required
                        autoFocus
                        id="email"
                        name="email"
                        class="form-control" 
                        value={email}
                        onChange={this.handleChange}></input> 
                    </div>

                    <div class="form-group" id="password">
                         <label for="password">Password</label>
                        <input type="password"
                        required 
                        id="password"
                        name="password"  
                        class="form-control"
                        value={password}
                        onChange={this.handleChange}></input> 
                    </div >
        
                    <div  class="control" id="control">
                        <a href="Admin.html" class="btn btn-outline-secondary mr-2 mb-4">Cancel</a>
                        <button type="submit" class="btn btn-outline-primary mb-4">UPDATE</button>
                    </div>
                 </form>
                    
            </div>
        </div>
            
        )}}