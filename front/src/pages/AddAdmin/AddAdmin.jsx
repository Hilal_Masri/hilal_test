import './AddAdmin.css'
const { Component } = require("react");

export default class AddAdmin extends Component{

    state = {
        
        email: "",
        password: "",
        error:false
    }

    createAdmin = async (params = {}) => {
        let { email, password } = params;
        let url = `//localhost:8000/Admins/create/?email=${email}&password=${password}`;
        let paramsErr = "you need at least name or email properties to update a contact";
        if (!email || !password) throw new Error(paramsErr);
        try{
            await fetch(url);
        }catch(err){
             this.setState({ error_message: err })
         }
        
    }
    handleChange = (event) => {
        let { name, value } = event.target;
        this.setState({ [name]: value });
    }
    
    handleSubmit = (event) => {
        event.nativeEvent.preventDefault();
        let { email, password } = this.state;
        this.createAdmin({ email, password });
        document.getElementById("email").value = "";
        document.getElementById("password").value = "";
    }

    

    render(){
        let {admins, email, password ,error} = this.state;
        return error ? (
            <div>
              <p>{error}</p>
              <button onClick={this.clearError}>refresh</button>
            </div>
            ) : (<div>
            <div class="container" id="container" >
                 <h1 class=" mb-4 text-light">NEW ADMIN</h1>
                 <form onSubmit={this.handleSubmit}>
                 <div class="form-group" id="Email">
                        <label for="email">Email</label>
                        <input type="email"
                        required
                        autoFocus
                        id="email"
                        name="email"
                        class="form-control" 
                        value={email}
                        onChange={this.handleChange}></input> 
                    </div>

                    <div class="form-group" id="password">
                         <label for="password">Password</label>
                        <input type="password"
                        required 
                        id="password"
                        name="password"  
                        class="form-control"
                        value={password}
                        onChange={this.handleChange}></input> 
                    </div >
        
                    <div  class="control" id="control">
                        <a href="Admin.html" class="btn btn-outline-secondary mr-2 mb-4">Cancel</a>
                        <button type="submit" class="btn btn-outline-primary mb-4">Save</button>
                    </div>
                 </form>
                    
            </div>
        </div>)}}