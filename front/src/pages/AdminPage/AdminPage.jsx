import './AdminPage.css'

import AdminCard from '../../components/AdminCard/AdminCard.jsx' 
//import {Link} from "react-router"

const { Component } = require("react");

export default class AdminPage extends Component{

    state = {
        admins: [],
        email: "",
        password: "",
        error:false
    }

    async componentDidMount() {
        await this.getAdmins();
         
    }

    getAdmins = async () => {
        try {
          const response = await fetch('//localhost:8000/Admims');
          const result = await response.json();
          if (result.success) {
            const admins = result.result;
            this.setState({ admins });
          } else {
            const error = result.message;
            this.setState({ error });
          }
        } catch (err) {
          this.setState({ error_message: err })
        }
    }

    deleteAdmin = async id => {
        try {
    
          const response = await fetch(`//localhost:8000/Admin/delete/${id}`);
          const result = await response.json();
          
    
          if (result.success) {
    
            let stateAdmins = [...this.state.admins].filter(admin => admin.id != id);
            this.setState({ admins: stateAdmins });
    
          } else this.setState({ error: result.message });
    
        } catch (err) {
          this.setState({ error_message: err })
        }
      }

    render(){
        let {admins} = this.state;
        return(<div> 
            
            <div class="p-1 my-container active-cont">
                <div class="container" id="container">
                   <h1 class="mb-4" id="AdminTitle">ADMIN LIST</h1>
                   <div class="btn btn-outline-primary">
                   <span >Add New Admin</span>
                   </div>
                    

                    {admins.map(admin => (
                        <AdminCard
                        key={admin.id}
                        id={admin.id}
                        email={admin.email}
                        password={admin.password}
                        deleteAdmin={this.deleteAdmin}
                        />
                    ))}
                    
                    
                    
                 </div>
                 
              </div>
              
        </div>
            
        )}
}