const db =require('./db');
const app=require('./app')

//db.createtables();
//db.deleteTable()

const start = async () => {

    const initializeDatabase = await db.initializeDatabase();
    const controllerBlog= await initializeDatabase.blogsController;
    const controllerAdmin=await initializeDatabase.adminsController;

    app.get('/', (req, res) => res.send("ok"));

    // CREATE blogs
    app.post('/blogs/create', async (req, res, next) => {
        const { title, description, content, image } = req.body;
        try{
        const result = await controllerBlog.createBlog({ title, description, content, image });
        res.json({ success: true, result });
        }catch(e){
            next(e);
        }
    })

    // LIST blogs
    app.get('/blogs', async (req, res, next) => {
        try{
        const blogs = await controllerBlog.getBlogsList()
        res.json({ success: true, result: blogs })
        }catch(e){
            next(e);
        }
    })

    // LIST BY TITLE blogs
    app.get('/blogs/byTitle', async (req, res, next) => {
        const {title}=req.body
        try{
        const blogs = await controllerBlog.getBlog(title)
        res.json({ success: true, result: blogs })
        }catch(e){
            next(e);
        }
    })

    // LIST BY id blogs
    app.get('/blogs/:id', async (req, res, next) => {
        const {id}=req.params
        try{
        const blogs = await controllerBlog.getBlogById(id)
        res.json({ success: true, result: blogs })
        }catch(e){
            next(e);
        }
    })

    // LIST BY top 4 views blogs
    app.get('/blogs/byViews', async (req, res, next) => {
        try{
        const blogs = await controllerBlog.getTopViewsBlog()
        res.json({ success: true, result: blogs })
        }catch(e){
            next(e);
        }
    })

    // LIST BY top feature blogs
    app.get('/blogs/byFeature', async (req, res, next) => {
        try{
        const blogs = await controllerBlog.getBlogFeature()
        res.json({ success: true, result: blogs })
        }catch(e){
            next(e);
        }
    })

    //update blogs
    app.put('/blogs/update/:id',async (req, res, next)=>{
        const {id}=req.params
        const {title, image, description, content,feature}=req.body
        try{
        const result=await controllerBlog.updateBlog(id, {title, image, description, content,feature})
        res.json({ success:true, result})
        }catch(e){
            next(e);
        }
    })

    //delete blogs
    app.delete('/blogs/delete/:id',async (req, res, next)=>{
        const {id}=req.params
        try{
        const result = await controllerBlog.deleteBlog(id)
        res.json({ success: true, result })
        }catch(e){
            next(e);
        }

    })

    //List Admin
    app.get('/Admims',async (req,res,next)=>{
        try{
            const admins=await controllerAdmin.getAdminList();
            res.json({success:true, result:admins})
        }catch(e){
            next(e);
        }
    })


    //Create Admin
    app.get('/Admins/create', async(req,res,next)=>{
        const {email,password}=req.query
        try{
            const admin=await controllerAdmin.createAdmin({email,password});
            res.json({success:true,result:admin})
        }catch(e){
            next(e);
        }
    })

    //Update Admin
    app.put('/Admins/update/:id', async(req,res,next)=>{
        try{
            const {id}=req.paramas;
            const {email,password}=req.body;
            const result=await controllerAdmin.updateAdmin(id,{email,password});
            res.json({success:true,result})
        }catch(e){
            next(e);
        }
    })

    //delete Admin
    app.get('/Admin/delete/:id',async(req,res,next)=>{
        try{
            const {id}=req.params;
            const result=await controllerAdmin.deleteAdmin(id)
            res.json({success:true,result})
        }catch(e){
            next(e);
        }
    })

}
start()

app.listen( 8000, () => console.log('server listening on port 8000') )
