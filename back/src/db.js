const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");
const SQL = require("sql-template-strings");
const app = require("./app");

const createtables = async () => {
    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    })

    /**
   * Create the table
   **/
    await db.run(`CREATE TABLE admin (id INTEGER PRIMARY KEY
    AUTOINCREMENT, email VARCHAR(55) NOT NULL, password VARCHAR(25) NOT NULL);`);

    await db.run(`CREATE TABLE contact (id INTEGER PRIMARY KEY
    AUTOINCREMENT, name VARCHAR(55) NOT NULL, email VARCHAR(55) NOT NULL,
    title VARCHAR(55) NOT NULL, message text NOT NULL);`);

    await db.run(`CREATE TABLE subscriber (id INTEGER PRIMARY KEY
    AUTOINCREMENT, email VARCHAR(55) NOT NULL);`);

    await db.run(`CREATE TABLE blog (id INTEGER PRIMARY KEY
    AUTOINCREMENT, title VARCHAR(55) NOT NULL, description text NOT NULL,
    content TEXT NOT NULL, image TEXT NOT NULL,
    views INTEGER NOT NULL, feature INTEGER, date DATETIME NOT NULL);`);

    await db.run(`CREATE TABLE comments (id INTEGER PRIMARY KEY
    AUTOINCREMENT, desc TEXT NOT NULL, date DATETIME NOT NULL,
    idblog INTEGER NOT NULL,FOREIGN KEY (idblog) REFERENCES blog(id));`);

    await db.run(`CREATE TABLE IP (id INTEGER PRIMARY KEY
    AUTOINCREMENT, IP VARCHAR(55) NOT NULL, idblog text NOT NULL,
    FOREIGN KEY (idblog) REFERENCES blog(id));`);
    console.log('tables created!')
}

//delete the tables
const deleteTable = async () => {
    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    })
    const tableName = [`admin`, `contact`, `subscriber`, `blog`, `comments`, `IP`];
    let statement = `DROP TABLE `
    tableName.forEach(async (t) => {
        let sta = statement + t;
        await db.run(sta);

    })
    console.log('tables deleted!');

}

const initializeDatabase = async () => {

    const db = await sqlite.open({
        filename: 'db.sqlite',
        driver: sqlite3.Database
    })
    // retrieves the blogs from the database
    const getBlogsList = async () => {
        let statement = `SELECT  title, image, description, views, date FROM blog ORDER BY date DESC`
        try {
            const rows = await db.all(statement);
            if (!rows.length) throw new Error(`no rows found`);
            return rows;
        } catch (e) {
            throw new Error(`couldn't retrieve blogs: ` + e.message);
        }
    }
    //select blog by title
    const getBlog = async (title) => {
        let statement = `SELECT title, description, image FROM blog, views, date WHERE title LIKE'%${title}%'`
        const blog = await db.get(statement);
        if (!blog) throw new Error(`title: ${title} not found`);
        return blog;
    }
    //select blog by id
    const getBlogById = async (id) => {
        let statement = `SELECT title, content, image FROM blog WHERE id = ${id}`
        const blog = await db.get(statement);
        if (!blog) throw new Error(`blog not found`);
        return blog;
    }

    //add new blog
    const createBlog = async (props) => {
        if (!props || !props.title || !props.description || !props.content || !props.image || !props.feature) {
            throw new Error(`you must provide a title and an description,content,image`);
        }
        const { title, description, content, image, feature } = props;
        try {
            const result = await db.run(`INSERT INTO blog (title,description,content,image,feature) VALUES (?, ?, ?, ?, ?)`, [title, description, content, image, feature]);
            const id = result.lastID;
            return id;
        } catch (e) {
            throw new Error(`couldn't insert this combination: ` + e.message);
        }
    }
    //edit blog
    const updateBlog = async (id, props) => {
        if (!props && !(props.title && props.image && props.description && props.content && props.feature)) {
            throw new Error(`you must provide a title or a image or a description or a content`);
        }
        const { title, image, description, content, feature } = props;
        let stmt, params = [];
        if (title && image && description && content && feature) {
            stmt = `UPDATE blog SET title = ?, image = ?, description= ?, content= ?, feature=?  WHERE id = ?`;
            params = [title, image, description, content, feature, id];
        }
        else if (title && !image && description && content && feature) {
            stmt = `UPDATE blog SET title = ?, description= ?, content= ?, feature=?  WHERE id = ?`;
            params = [title, description, content, feature, id];
        }
        // if (title && image && description && content &&feature) {
        //     stmt = `UPDATE blog SET title = ?, image = ?, description= ?, content= ?, feature=?  WHERE id = ?`;
        //     params = [title, image, description, content, feature, id];
        // }
        // else if (!title && image && description && content &&feature) {
        //     stmt = `UPDATE blog SET  image = ?, description= ?, content= ?, feature=?  WHERE id = ?`;
        //     params = [image, description, content,feature, id];
        // }
        // else if (title && !image && description && content &&feature) {
        //     stmt = `UPDATE blog SET title = ?, description= ?, content= ?, feature=?  WHERE id = ?`;
        //     params = [title, description, content,feature, id];
        // }
        // else if (title && image && !description && content &&feature) {
        //     stmt = `UPDATE blog SET title = ?, image = ?, content= ?, feature=?  WHERE id = ?`;
        //     params = [title, image, content,feature, id];
        // }
        // else if (title && image && description && !content &&feature) {
        //     stmt = `UPDATE blog SET title = ?, image = ?, description= ?, feature=? WHERE id = ?`;
        //     params = [title, image, description,feature, id];
        // }
        // else if (title && image && description && content && !feature) {
        //     stmt = `UPDATE blog SET title = ?, image = ?, description= ? WHERE id = ?`;
        //     params = [title, image, description, id];
        // }
        // else if (!title && !image && description && content) {
        //     stmt = `UPDATE blog SET description= ?, content= ?  WHERE id = ?`;
        //     params = [description, content, id];
        // }
        // else if (!title && image && !description && content) {
        //     stmt = `UPDATE blog SET image = ?, content= ?  WHERE id = ?`;
        //     params = [image, content, id];
        // }
        // else if (!title && image && description && !content) {
        //     stmt = `UPDATE blog SET image = ?, description= ? WHERE id = ?`;
        //     params = [image, description, id];
        // }
        // else if (title && !image && !description && content) {
        //     stmt = `UPDATE blog SET title = ?, content= ?  WHERE id = ?`;
        //     params = [title, content, id];
        // }
        // else if (title && !image && description && !content) {
        //     stmt = `UPDATE blog SET title = ?, description= ? WHERE id = ?`;
        //     params = [title, description, id];
        // }
        // else if (title && image && !description && !content) {
        //     stmt = `UPDATE blog SET title = ?, image = ? WHERE id = ?`;
        //     params = [title, image, id];
        // }

        try {
            const result = await db.run(stmt, params);
            if (result.changes === 0) throw new Error(`no changes were made`);
            return true;
        } catch (e) {
            throw new Error(`couldn't update the contact ${id}: ` + e.message);
        }
    }
    //delete blog
    const deleteBlog = async (id) => {
        try {
            const result = await db.run(`DELETE FROM blog WHERE id = ?`, id);
            if (result.changes === 0) throw new Error(`blog "${id}" does not exist`);
            return true;
        } catch (e) {
            throw new Error(`couldn't delete the blog "${id}": ` + e.message);
        }
    }
    //select top 4 blog (nb views)
    const getTopViewsBlog = async () => {
        let statement = `SELECT  title, image, description, views, date FROM blog ORDER BY views DESC LIMIT 4`
        try {
            const rows = await db.all(statement);
            if (!rows.length) throw new Error(`no rows found`);
            return rows;
        } catch (e) {
            throw new Error(`couldn't retrieve blogs: ` + e.message);
        }
    }

    //select blog by feature
    const getBlogFeature = async () => {
        let statement = `SELECT title, description, image FROM blog, views, date WHERE feature=1`
        const blog = await db.get(statement);
        if (!blog) throw new Error(`not found`);
        return blog;
    }
    //retrives the Admins 
    const getAdminList = async () => {
        let statement = `SELECT id, email, password from admin ORDER BY email ASC`
        try {
            const rows = await db.all(statement)
            if (!rows.length) throw new Error(`no rows found`);
            return rows
        } catch (e) {
            throw new Error(`couldn't retrive Admins` + e.message)
        }
    }

    //add new Admin
    const createAdmin = async (props) => {
        if (!props || !props.email || !props.password) {
            throw new Error(`you must provide a email and a password`)
        }
        const { email, password } = props;
        try {
            const result = await db.run(`INSERT INTO admin (email,password) VALUES (?, ?)`, [email, password])
            const id = result.lastID;
            return id;

        } catch (e) {
            throw new Error(`couldn't insert this combination` + e.message)
        }
    }

    //update Admin
    const updateAdmin = async (id, props) => {
        if (!props && !(props.email && props.password)) {
            throw new Error(`you must provide a email or an password`)
        }
        if (email && password) {
            let stmt = `UPDATE admin SET email=?, password=? WHERE id=?`
            params = [email, password, id]
        }
        else if (!email && password) {
            let stmt = `UPDATE admin SET  password=? WHERE id=?`
            params = [password, id]
        }
        else if (email && !password) {
            let stmt = `UPDATE admin SET email=? WHERE id=?`
            params = [email, id]
        }
        try {
            const result = await db.run(stmt, params)
            if (result.changes === 0) throw new Error(`no changes were made`)
            return true;
        } catch (e) {
            throw new Error(`could'nt update the Admin ${id}` + e.message)
        }
    }

    //Delete Admin
    const deleteAdmin = async (id) => {
        try {
            const result = await db.run(`DELETE FROM admin WHERE id=?`, id)
            if (result.changes === 0) throw new Error(`Admin does not exists`)
            return true;

        } catch (e) {
            throw new Error(`couldn't delete the Admin` + e.message)
        }
    }



    const blogsController = {
        getBlogsList,
        getBlog,
        getBlogById,
        createBlog,
        updateBlog,
        deleteBlog,
        getTopViewsBlog,
        getBlogFeature
    }

    const adminsController = {
        getAdminList,
        createAdmin,
        updateAdmin,
        deleteAdmin
    }

    const controller = {
        blogsController,
        adminsController
    }

    return controller;


}

//module.exports={createtables,deleteTable}
const db = { initializeDatabase }
module.exports = db;